<?php

/**
 * Mitsar framework
 *
 * Основной индекс файл фреймворка, через который происходят все вызовы внутри ядра.
 */

// define('TWIG_DISABLE_CACHE', true);
define('SYSTEM_ENABLE_DEBUG', true);
define('SYSTEM_START_TIME', microtime(TRUE));

// подключаем ядро
require_once(".system/start.php");

\Kernel\Site::Start()->Display();

/// Если объевлена константа SYSTEM_START_TIME с microtime(TRUE)
/// то будет показана разница между текущим временем и SYSTEM_START_TIME
pre(Classes\Timer::TimeDiff());