<?php

namespace Classes;

class String {

    const STR_EMPTY = "";

    private $origin_string = null;
    private $result_string = null;

    function __construct($string)
    {
        $this->origin_string = $string;
        $this->result_string = $this->origin_string;
    }

    public function Trim()
    {
        $this->result_string = self::mb_trim($this->result_string);
        return $this->result_string;
    }

    public function StartWith($string)
    {
        return (mb_substr($this->result_string, 0, strlen($string), SYSTEM_MBSTRING_DEFAULT_CHARSET) === $string);
    }

    public function EndWith($string)
    {
        $endlen = mb_strlen($string, SYSTEM_MBSTRING_DEFAULT_CHARSET);
        $strlen = mb_strlen($this->result_string, SYSTEM_MBSTRING_DEFAULT_CHARSET);
        return (mb_substr($this->result_string, $strlen - $endlen, $endlen, SYSTEM_MBSTRING_DEFAULT_CHARSET) === $string);
    }

    public function Contains($string)
    {
        return (mb_strpos($this->result_string, $string, null, SYSTEM_MBSTRING_DEFAULT_CHARSET) !== false);
    }

    public function Replace($search, $string)
    {
        $this->result_string = self::mb_str_replace($search, $string, $this->result_string);
        return $this->result_string;
    }

    public function Equal($eqstring)
    {
        return ($this->result_string === $eqstring);
    }

    public function NotEqual($eqstring)
    {
        return ($this->result_string !== $eqstring);
    }

    public function returnString()
    {
        return $this->result_string;
    }

    public function reverse()
    {
        return self::strrev($this->result_string);
    }

    #region Static method

    static public function strrev($source, $charset = SYSTEM_MBSTRING_DEFAULT_CHARSET)
    {
        $output_string = "";
        $string_length = mb_strlen($source, $charset);

        for($i = $string_length - 1; $i >= 0; --$i)
            $output_string .= mb_substr($source, $i, 1, $charset);

        return $output_string;
    }

    static public function mb_trim($string)
    {
        $string = preg_replace("/(^\s+)|(\s+$)/us", "", $string);

        return $string;
    }

    static public function mb_str_replace($search, $replace, $subject, &$count = 0) {
        if (!is_array($subject)) {
            // Normalize $search and $replace so they are both arrays of the same length
            $searches = is_array($search) ? array_values($search) : array($search);
            $replacements = is_array($replace) ? array_values($replace) : array($replace);
            $replacements = array_pad($replacements, count($searches), '');

            foreach ($searches as $key => $search) {
                $parts = mb_split(preg_quote($search), $subject);
                $count += count($parts) - 1;
                $subject = implode($replacements[$key], $parts);
            }
        } else {
            // Call mb_str_replace for each subject in array, recursively
            foreach ($subject as $key => $value) {
                $subject[$key] = self::mb_str_replace($search, $replace, $value, $count);
            }
        }

        return $subject;
    }

    #endregion

    public function __invoke($string) {
        $this->origin_string = $string;
        $this->result_string = $this->origin_string;
        return $this;
    }

    public function __toString()
    {
        return $this->returnString();
    }

    public static function Override($string)
    {
        return new self($string);
    }

}