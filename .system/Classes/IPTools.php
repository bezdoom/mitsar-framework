<?php

namespace Classes;

/**
 * IPTools
 * @desc Вспомогательный класс для работы с IP адресами
 */

class IPTools
{

    const MSG_FORBIDDEN = "Forbidden";

    // Список IP адресов
    private $ipList = array();

    function __construct()
    {
        $list = func_get_args();

        if (!empty($list))
        {
            if (isset($list[0]) && is_array($list[0]))
            {
                $list = $list[0];
            }
        }

        foreach ($list as $ip)
        {
            if (self::ValidateIP($ip))
                $this->ipList[] = $ip;
        }
    }

    /// <summary>
    /// Запрещает доступ (403) для всех IP адресов, кроме тех, которые есть в списке
    /// </summary>
    public function AccessAllow()
    {
        if (!$this->Find(self::CurrentIP()))
            self::Forbidden();
    }

    /// <summary>
    /// Запрещает доступ (403) для IP адресов из текущего списка
    /// </summary>
    public function AccessDenied()
    {
        if ($this->Find(self::CurrentIP()))
            self::Forbidden();
    }

    /// <summary>
    /// Осуществляет поиск IP адреса в списке объекта
    /// </summary>
    public function Find($ipAddress)
    {
        foreach($this->ipList as $ip)
        {
            if ($ip == $ipAddress)
                return true;
        }

        return false;
    }

    /// <summary>
    /// Возвращает IP адрес текущего посетителя
    /// </summary>
    static function CurrentIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    /// <summary>
    /// Останавливает выполнение кода и выводит 403 ошибку
    /// </summary>
    static public function Forbidden($message = null)
    {
        header('HTTP/1.1 403 Forbidden');

        if ($message === null || $message == false)
        {
            $message = self::MSG_FORBIDDEN;
        }

        print $message;
        exit;
    }

    /// <summary>
    /// Останавливает проверку на корректность IP адреса (v4 и v6)
    /// </summary>
    static public function ValidateIP($ip = null)
    {
        $ip = ($ip === null || $ip == false) ? self::CurrentIP() : $ip;
        return inet_pton($ip) !== false;
    }

    /// <summary>
    /// Возвращает экземпляр объекта
    /// </summary>
    static public function Node()
    {
        return new self(func_get_args());
    }

    /// <summary>
    /// Возвращает IP адрес текущего посетителя
    /// </summary>
    public function __toString()
    {
        return self::CurrentIP();
    }

    /// <summary>
    /// Возвращает количество адресов в списке
    /// </summary>
    public function Count()
    {
        return Count($this->ipList);
    }

}