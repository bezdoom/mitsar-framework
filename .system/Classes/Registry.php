<?PHP

namespace Classes;

class Registry
{
    /**
     * Контейнер с элементами реестра
     * @var array
     */
    static private $container = array();

    /**
     * Регистрирует в реестре новый объект
     * @param $key
     * @param $value
     * @throws \Exception
     */
    static public function Set($key, $value) {
        if (!self::Exists($key))
            self::$container[$key] = $value;
        else
            throw new \Exception("Key \"".$key."\" is exists in Registry.");
    }

    /**
     * Возвращает именованый объект из реестра
     * @param $key
     * @return mixed
     * @throws \Exception
     */
    static public function Get($key) {
        if (!self::Exists($key))
            throw new \Exception("Key \"".$key."\" does not exists in Registry.");
        else
            return self::$container[$key];
    }

    /**
     * Обновляет значение массива в реестре
     * @param $key
     * @param array $value
     * @throws \Exception
     */
    static public function UpdateArray($key, array $value)
    {
        if (self::Exists($key))
            if (is_array(self::$container[$key]))
                self::$container[$key][$value[0]] = $value[1];
            else
                throw new \Exception("Key \"".$key."\" exists, but it does not instance of array.");
        else
            throw new \Exception("Key \"".$key."\" does not exists in Registry.");
    }

    /**
     * Удаляет значение из реестра
     * @param $key
     * @throws \Exception
     */
    static public function Delete($key)
    {
        if (self::Exists($key))
            unset(self::$container[$key]);
        else
            throw new \Exception("Key \"".$key."\" does not exists in Registry.");
    }

    /**
     * Проверяет, существует ли значение с указанным ключем в реестре
     * @param $key
     * @return bool
     */
    static public function Exists($key)
    {
        return array_key_exists($key, self::$container);
    }

}