<?php

namespace Classes;

class DateTime extends \DateTime
{

    const DATE_FORMAT = DEFAULT_DATE_FORMAT;
    const TIME_FORMAT = DEFAULT_TIME_FORMAT;
    const DATETIME_FORMAT = DEFAULT_DATETIME_FORMAT;
    const TIME_ZONE = DEFAULT_TIMEZONE;

    const GMT_TIMEZONE = "Etc/GMT";
    const GMT_DATETIME_FORMAT = "D, d M Y H:i:s T";

    public $days = null;
    public $month = null;
    public $year = null;

    public $hours = null;
    public $minutes = null;
    public $seconds = null;

    public function __construct($time = "now", $time_zone = null){
        if(is_null($time_zone) || !($time_zone instanceof \DateTimeZone))
            $time_zone = new \DateTimeZone(self::TIME_ZONE);

        parent::__construct($time, $time_zone);

        $this->__update();
    }

    private function __update() {
        $this->days = $this->format("d");
        $this->month = $this->format("m");
        $this->year = $this->format("Y");
        $this->hours = $this->format("H");
        $this->minutes = $this->format("i");
        $this->seconds = $this->format("s");
    }

    public function __toString() {
        return $this->format(self::DATETIME_FORMAT);
    }

    public function getTimestamp(){
        return $this->format ("U");
    }

    /**
     * DateTime modificators
     */

    public function plusDays($days) {
        $this->modify("+".$days." days");
    }
    public function minusDays($days) {
        $this->modify("-".$days." days");
    }

    public function plusMonth($month) {
        $this->modify("+".$month." month");
    }
    public function minusMonth($month) {
        $this->modify("-".$month." month");
    }

    public function plusYear($year) {
        $this->modify("+".$year." year");
    }
    public function minusYear($year) {
        $this->modify("-".$year." year");
    }

    public function plusMinutes($minutes) {
        $this->modify("+".$minutes." minutes");
    }
    public function minusMinutes($minutes) {
        $this->modify("-".$minutes." minutes");
    }

    public function plusHours($hours) {
        $this->modify("+".$hours." hours");
    }
    public function minusHours($hours) {
        $this->modify("-".$hours." hours");
    }

    public function plusSeconds($seconds) {
        $this->modify("+".$seconds." seconds");
    }
    public function minusSeconds($seconds) {
        $this->modify("-".$seconds." seconds");
    }

    // override modify
    public function modify($string) {
        parent::modify($string);
        $this->__update();
    }

    /**
     *    This function calculates the number of days between the first and the second date. Arguments must be subclasses of DateTime
     **/
    static public function differenceInDays (\Classes\DateTime $firstDate, \Classes\DateTime $secondDate){
        $firstDateTimeStamp = $firstDate->format("U");
        $secondDateTimeStamp = $secondDate->format("U");
        $rv = round ((($firstDateTimeStamp - $secondDateTimeStamp))/86400);
        return $rv;
    }

    static public function Now($tzone = null){
        $dt_zone = (!is_null($tzone)) ? new \DateTimeZone($tzone) : null;
        return new self("now", $dt_zone);
    }

}