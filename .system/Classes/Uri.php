<?php

namespace Classes;

/**
 * URI Universal class
 */
class Uri
{
    static protected $instance = null;

    static public function Node()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // === begin class ===

    const REGEXP_PATTERN = "/(?:([^\:]*)\:\/\/)?(?:([^\:\@]*)(?:\:([^\@]*))?\@)?(?:([^\/\:]*)\.(?=[^\.\/\:]*\.[^\.\/\:]*))?([^\.\/\:]*)(?:\.([^\/\.\:]*))?(?:\:([0-9]*))?(\/[^\?#]*(?=.*?\/)\/)?([^\?#]*)?(?:\?([^#]*))?(?:#(.*))?/";

    /**
     * @var array
     * Массив связей совпадений регулярного выражения с объектом
     */
    private $assignation = array(
        0 => "origin",
        1 => "protocol",
        2 => "auth_login",
        3 => "auth_passwd",
        4 => "subdomain",
        5 => "domain_name",
        6 => "domain_zone",
        7 => "port",
        8 => "path",
        9 => "file",
        10 => "query",
        11 => "hash"
    );

    /**
     * @var string
     * Оригинальный адрес (https://server.com/dir1/dir2/test.html?action=test#hashtag)
     *
     * Example: https://server.com/dir1/dir2/test.html?action=test#hashtag
     */
    public $origin = null;

    /**
     * @var string
     * Протокол (http://www.yandex.ru)
     *
     * Exaple: http
     */
    public $protocol = null;

    /**
     * @var string
     * Логин для авторизации (ftp://admin:pass@server.com)
     *
     * Example: admin
     */
    public $auth_login = null;

    /**
     * @var string
     * Пароль для авторизации (ftp://admin:pass@server.com)
     *
     * Example: pass
     */
    public $auth_passwd = null;

    /**
     * @var string
     * Поддомен (http://test.sochi2014.com)
     *
     * Example: test
     */
    public $subdomain = null;

    /**
     * @var string
     * Имя основного домена (http://test.server.com)
     *
     * Example: server
     */
    public $domain_name = null;

    /**
     * @var string
     * Зона домена (http://test.server.com)
     *
     * Example: com
     */
    public $domain_zone = null;

    /**
     * @var string
     * Имя основного домена (http://test.server.com)
     *
     * Example: server.com
     */
    public $domain_origin = null;

    /**
     * @var string
     * Имя домена (http://test.server.com)
     *
     * Example: test.server.com
     */
    public $domain = null;

    /**
     * @var string
     * Порт (https://server.com:443)
     *
     * Example: 443
     */
    public $port = null;

    /**
     * @var string
     * Путь (https://server.com/dir1/dir2/test.html)
     *
     * Example: /dir1/dir2/
     */
    public $path = null;

    /**
     * @var string
     * Имя файла (https://server.com/dir1/dir2/test.html)
     *
     * Example: test.html
     */
    public $file = null;

    /**
     * @var string
     * Полный путь (https://server.com/dir1/dir2/test.html)
     *
     * Example: /dir1/dir2/test.html
     */
    public $full_path = null;

    /**
     * @var string
     * Запрос (https://server.com/dir1/dir2/test.html?action=test)
     *
     * Example: ?action=test
     */
    public $query = null;

    /**
     * @var string
     * Полный путь и запрос (https://server.com/dir1/dir2/test.html?action=test)
     *
     * Example: /dir1/dir2/test.html?action=test
     */
    public $path_and_query = null;

    /**
     * @var string
     * Хеш строки (https://server.com/dir1/dir2/test.html?action=test#hashtag)
     *
     * Example: hashtag
     */
    public $hash = null;

    /**
     * @var array
     * Сегменты пути (https://server.com/dir1/dir2/test.html?action=test#hashtag)
     *
     * Example: array(0=>"dir1", 1=>"dir2", 2=>"test.html")
     */
    public $segments = array();

    /**
     * Разобрать оригинальную ссылку на сегменты
     */
    private function  __ParseUrl($url) {
        $matches = array();
        try {
            // Парсинг ссылки с помощью регулярного выражения
            preg_match_all(self::REGEXP_PATTERN, $url, $matches);
            foreach($matches as $id => $match) {
                $var = $this->assignation[$id];
                $this->{$var} = $match[0];
            }

            // Построение дополнительных переменных
            $this->domain_origin = $this->domain_name . "." . $this->domain_zone;
            $this->domain = $this->subdomain . "." . $this->domain_origin;
            $this->full_path = str_replace("//", "/", $this->path . "/" . $this->file);
            $this->path_and_query = $this->full_path . (($this->query != false) ? ("?" . $this->query) : "");

            // Разбивание на сегменты
            $segments = explode("/" , $this->full_path);
            foreach($segments as $id => $segment) {
                if (!($id == 0 && $segment == false)
                    && !($id == count($segments)-1 && $segment == false))
                    $this->segments[] = $segment;
            }
        }catch (\Exception $Ex) {
            print $Ex->getMessage();
        }

    }

    /**
     * @param null $url
     * Конструктор
     */
    function __construct($url = null) {
        if (is_null($url)) {
            $protocol = self::IsSecureConnection() ? "https://" : "http://";
            $parseUrl = $protocol . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        } else {
            $parseUrl = Trim($url);
        }

        $this->__ParseUrl($parseUrl);
    }

    public static function Current($url = null) {
        return new self($url);
    }

    public static function IsSecureConnection() {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443);
    }

}