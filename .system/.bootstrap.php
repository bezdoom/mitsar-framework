<?php

// check server php version and stop if need
if (defined("PHP_VERSION_MIN"))
{
    $install_php_version = phpversion();
    if (version_compare($install_php_version, PHP_VERSION_MIN) < 0)
    {
        die("Unsupported PHP version: ".$install_php_version."<br /><br />\r\nMinimum: ".PHP_VERSION_MIN);
    }
}

// enable debug message, if need
if (defined("SYSTEM_ENABLE_DEBUG")
    && constant("SYSTEM_ENABLE_DEBUG") === true)
{
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 'On');
    ini_set('log_errors', 'On');
}

// check cache directory
// and create it if need + touch .htaccess file
if (defined("SYSTEM_CREATE_CACHE_FOLDER")
    && constant("SYSTEM_CREATE_CACHE_FOLDER") === true)
{
    $cachedir = SYSTEM_ROOT_DIRECTORY . SYSTEM_CACHE_DIRECTORY . DS;

    if (!file_exists($cachedir))
    {
        mkdir($cachedir, 0777, true);
    }

    touchHtaccess($cachedir);
}

// enable application environment
if (!defined("CURRENT_APPLICATION"))
{
    define("CURRENT_APPLICATION", SYSTEM_DEFAULT_APPLICATION);
}

// require bootstrap for application
$appBootstrapFile = __DIR__ . "/../" . CURRENT_APPLICATION . '/.configuration/.bootstrap.php';
if (file_exists($appBootstrapFile))
{
    require_once ($appBootstrapFile);
}