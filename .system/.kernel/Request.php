<?php

namespace Kernel;

class Request
{
    const VALID_INT                 = 0x001;
    const VALID_FLOAT               = 0x002;
    const VALID_EMAIL               = 0x003;

    static protected $instance = null;

    static public function Node()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // === begin class ===

    protected function inputValidation($value, $validation)
    {
        switch($validation)
        {
            case self::VALID_INT:
                return filter_var($value, FILTER_VALIDATE_INT);
                break;
            case self::VALID_EMAIL:
                return filter_var($value, FILTER_VALIDATE_EMAIL);
                break;
            case self::VALID_FLOAT:
                return filter_var($value, FILTER_VALIDATE_FLOAT);
                break;

            default:
                return filter_var($value, FILTER_DEFAULT);
        }
    }

    protected function extractValue($name, array $array, $default_value = "", $validation = null)
    {
        $_value = isset($array[$name]) ? $array[$name] : $default_value;

        if (!is_null($validation))
            $_value = $this->inputValidation($_value, $validation);

        return $_value;
    }

    public function get($name, $validation = null, $default_value = "")
    {
        return $this->extractValue($name, is_array($_GET) ? $_GET : array(), $default_value, $validation);
    }

    public function getPost($name, $validation = null, $default_value = "")
    {
        return $this->extractValue($name, is_array($_POST) ? $_POST : array(), $default_value, $validation);
    }
}