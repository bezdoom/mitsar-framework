<?php

namespace Kernel;

class Controller extends DataAdapter
{
    /**
     * @param $name
     * @param $value
     * @desc Добавляет данные в модель шаблонизатора
     */
    protected function SetVar($name, $value)
    {
        \Classes\Registry::UpdateArray("TWIG_MODEL", array($name, $value));
    }
}