<?php

namespace Kernel;

class Rule
{
    private $_var = array();

    public $controller = CONTROLLER_DEFAULT_NAME;
    public $action = CONTROLLER_DEFAULT_ACTION;

    function __construct($parameters = array(), $matches = array())
    {
        foreach($parameters as $name => $value)
        {
            $this->{$name} = $value;

            foreach($matches as $id => $vl)
            {
                $this->{$name} = str_replace("$".$id, $vl, $this->{$name});
            }
        }
    }

    // Методы
    public  function __set($name, $value)
    {
        $this->_var[$name] = $value;
    }

    public  function __get($name)
    {
        if (isset($this->_var[$name])) return $this->_var[$name];
        return '';
    }

    public function GetCustomPropertyList()
    {
        return $this->_var;
    }

}