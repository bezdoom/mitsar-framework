<?php

namespace Kernel;

class Response extends \Kernel\ResponseLayer
{
    /**
     * Вызывает рендер шаблона Twig на основе существующих параметров
     * @return mixed
     */
    public function Render()
    {
        return \Classes\Registry::Get("TWIG_STRING")->render($this->response, $this->replace);
    }
}