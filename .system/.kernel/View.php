<?php

namespace Kernel;

class View extends \Kernel\ResponseLayer
{
    /**
     * Вызывает рендер шаблона Twig на основе существующих параметров
     * @return mixed
     */
    public function Render()
    {
        return \Classes\Registry::Get("TWIG")->render($this->response, $this->replace);
    }
}