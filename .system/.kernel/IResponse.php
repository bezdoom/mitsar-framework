<?php

namespace Kernel;

interface IResponse
{
    function Render();
}