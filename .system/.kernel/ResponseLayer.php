<?php

namespace Kernel;

abstract class ResponseLayer implements \Kernel\IResponse
{
    protected $response = "";
    protected $replace = array();

    function __construct($response, $replace = null)
    {
        $this->response = $response;

        if (is_array($replace))
            $this->replace = $replace;
        else
            $this->replace = \Classes\Registry::Get("TWIG_MODEL");
    }

    abstract public function Render();
}