<?php

namespace Kernel;

/**
 * основная прослойка ядра, содержащая в себе все основные экземпляры объектов.
 */

class DataAdapter
{
    /**
     * @var \Classes\Uri
     *
     * Current URI
     */
    protected $uri = null;

    /**
     * @var \Kernel\db\PDO
     *
     * PDO объект
     */
    protected $pdo = null;

    /**
     * @var \Kernel\Request
     *
     * Request object
     */
    protected $request = null;

    /**
     * @var \Kernel\AppSettings
     *
     * Settings object
     */
    protected $settings = null;

    /**
     * @var \Kernel\Response
     *
     * Response object
     */
    protected $response = null;

    /**
     * @var \Kernel\Caching\Manager
     *
     * Cache object
     */
    protected $cache = null;

    function __construct()
    {
        $this->settings = is_null($this->settings) ? \Classes\Registry::Get("SETTINGS") : null;
        $this->uri = is_null($this->uri) ? \Classes\Registry::Get("URI") : null;
        $this->pdo = is_null($this->pdo) ? \Classes\Registry::Get("DB") : null;
        $this->request = is_null($this->request) ? \Classes\Registry::Get("REQUEST") : null;
        $this->response = is_null($this->response) ? \Classes\Registry::Get("RESPONSE") : null;
        $this->cache = is_null($this->cache) ? \Classes\Registry::Get("CACHE") : null;
    }
}