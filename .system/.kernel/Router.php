<?php

namespace Kernel;

use Kernel\Rule;

class Router extends DataAdapter
{
    private $rulesList = array();

    function __construct()
    {
        parent::__construct();
        $this->rulesList = $this->settings->RouteMapArray();
    }

    public function GetCurrentRule()
    {
        if ($this->uri->full_path == "/")
        {
            return new Rule();
        }
        else
        {
            foreach ($this->rulesList as $pattern => $param)
            {
                if (preg_match($pattern, $this->uri->full_path, $matches))
                {
                    return new Rule($param, $matches);
                }
            }

            // error 404 controller
            $errorController = new Rule();
            $errorController->controller = CONTROLLER_DEFAULT_ERROR_CONTROLLER;
            $errorController->action = CONTROLLER_DEFAULT_ERROR_ACTION_404;
            return $errorController;
        }
    }

    /**
     * @return Router
     */
    static public function Node()
    {
        return new self();
    }
}