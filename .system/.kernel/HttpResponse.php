<?php

namespace Kernel;

class HttpResponse
{
    static protected $instance = null;

    static public function Node()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // === begin class ===

    const HTTP_HEADER_403 = "HTTP/1.1 403 Forbidden";
    const HTTP_HEADER_404 = "HTTP/1.1 404 Not Found";
    const HTTP_HEADER_301_MOVED = "HTTP/1.1 301 Moved Permanently";
    const HTTP_HEADER_302_MOVED = "HTTP/1.1 302 Moved Temporarily";
    const HTTP_HEADER_302_FOUND = "HTTP/1.1 302 Found";
    const HTTP_HEADER_307_REDIRECT = "HTTP/1.1 307 Temporary Redirect";

    public function SetHeader($string, $replace = null, $http_response_code = null)
    {
        header($string, $replace, $http_response_code);
    }

    public function Redirect($location, $redirect = 302)
    {
        switch($redirect)
        {
            case 301:
                $this->SetHeader(self::HTTP_HEADER_301_MOVED);
                $this->SetHeader("Location: " . $location, true, 301);
                break;
            default:
                $this->SetHeader(self::HTTP_HEADER_302_MOVED);
                $this->SetHeader($location);
        }
        exit;
    }
}