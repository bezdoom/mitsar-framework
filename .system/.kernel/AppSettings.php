<?php

namespace Kernel;

class AppSettings {

    const DB_CONFIG_PATTERN = "db_{env}.php";
    const CONFIGURAION_DIRECTORY = "/../.configuration/";
    const CONFIGURAION_APP_DIRECTORY = "/../../{application}/.configuration/";

    static protected $instance = array();

    static public function Node($env = "prod")
    {
        if (!isset(self::$instance[$env]))
        {
            self::$instance[$env] = new self($env);
        }
        return self::$instance[$env];
    }

    // === begin class ===

    private $environemnt = null;

    private $cache_result = array();

    public $cfg_app_path = null;
    public $cfg_global_path = null;

    public function __construct($env = "prod")
    {
        $this->environemnt = ($env != null ? $env : "prod");
        $this->cfg_global_path = (__DIR__ . $this::CONFIGURAION_DIRECTORY);
        $this->cfg_app_path = (__DIR__ . str_replace("{application}", CURRENT_APPLICATION, $this::CONFIGURAION_APP_DIRECTORY));
    }

    private function _loadArrayReturn($filename)
    {
        $appSettings = array();
        $globalSettings = array();

        $appFile = $this->cfg_app_path . $filename;
        $globalFile = $this->cfg_global_path . $filename;

        if (file_exists($appFile))
        {
            $appSettings = (require $appFile);
        }

        if (file_exists($globalFile))
        {
            $globalSettings = (require $globalFile);
        }

        return array(
            "app" => is_array($appSettings) ? $appSettings : array(),
            "global" => is_array($globalSettings) ? $globalSettings : array()
        );
    }

    private function _mergeSettings($filename)
    {
        if (!isset($this->cache_result[$filename]))
        {
            $glob = $this->_loadArrayReturn($filename);
            $this->cache_result[$filename] = array_merge($glob["global"], $glob["app"]);
        }

        return $this->cache_result[$filename];
    }

    public function DBArray()
    {
        return $this->_mergeSettings(str_replace("{env}", $this->environemnt, $this::DB_CONFIG_PATTERN));
    }

    public function RouteMapArray()
    {
        return $this->_mergeSettings(".routing.php");
    }

    public function SiteArray()
    {
        return $this->_mergeSettings(".site.php");
    }

}