<?php

namespace Kernel;

class Site extends DataAdapter
{
    const CONTROLLER_PATTERN        = "Controllers\\{controller}Controller";

    const ACTION_PATTERN            = "_action{action}";
    const GET_ACTION_PATTERN        = "_get_action{action}";
    const POST_ACTION_PATTERN       = "_post_action{action}";
    const PUT_ACTION_PATTERN        = "_put_action{action}";
    const DELETE_ACTION_PATTERN     = "_delete_action{action}";
    const HEAD_ACTION_PATTERN       = "_head_action{action}";
    const OPTIONS_ACTION_PATTERN    = "_options_action{action}";
    const PATCH_ACTION_PATTERN      = "_patch_action{action}";

    const httpModule                = "httpModules";

    public $application = null;
    public $rule = null;
    public $action = null;
    public $controller = null;
    public $reflection = null;
    public $arguments = array();
    public $passion = array();
    public $site = array();

    private $error = false;

    function __construct()
    {
        $this->application = CURRENT_APPLICATION;
        // start datalayer
        parent::__construct();
        // continue
        $this->rule = \Kernel\Router::Node()->GetCurrentRule();
        $this->site = $this->settings->SiteArray();
        $this->arguments = $this->rule != null ? $this->rule->GetCustomPropertyList() : array();
        // httpModule onStart executing
        $this->_httpmodule_OnStartExecute();
        // setting default locale
        $this->_setLocale();
        // execute reflection method in using controller
        $this->_getController()->_getAction()->_reflectionMethod();
    }

    private function _getController()
    {
        $controllerName = str_replace("{controller}", ucfirst($this->rule->controller), $this::CONTROLLER_PATTERN);
        if (!class_exists($controllerName)){
            $this->error = true;
            $controllerName = str_replace("{controller}", CONTROLLER_DEFAULT_ERROR_CONTROLLER, $this::CONTROLLER_PATTERN);
        }

        $this->controller = new $controllerName();

        return $this;
    }

    private function _getAction()
    {
        // устанавливаем конкретизирующую привязку экшена к методу обращения
        switch($_SERVER['REQUEST_METHOD'])
        {
            case "POST":
                $methodName = str_replace("{action}", ucfirst($this->rule->action), $this::POST_ACTION_PATTERN);
                break;

            case "PUT":
                $methodName = str_replace("{action}", ucfirst($this->rule->action), $this::PUT_ACTION_PATTERN);
                break;

            case "DELETE":
                $methodName = str_replace("{action}", ucfirst($this->rule->action), $this::DELETE_ACTION_PATTERN);
                break;

            case "HEAD":
                $methodName = str_replace("{action}", ucfirst($this->rule->action), $this::HEAD_ACTION_PATTERN);
                break;

            case "OPTIONS":
                $methodName = str_replace("{action}", ucfirst($this->rule->action), $this::OPTIONS_ACTION_PATTERN);
                break;

            case "PATCH":
                $methodName = str_replace("{action}", ucfirst($this->rule->action), $this::PATCH_ACTION_PATTERN);
                break;

            // по умолчанию GET
            default:
                $methodName = str_replace("{action}", ucfirst($this->rule->action), $this::GET_ACTION_PATTERN);
        }

        // проверяем на наличие привязки экшена к конкретному методу,
        // если не установлена - возвращаем классическое имя экшена без уточнящей привязки
        if (!method_exists($this->controller, $methodName))
        {
            $methodName = str_replace("{action}", ucfirst($this->rule->action), $this::ACTION_PATTERN);

            // финальная проверка на доступнось искомого экшена в используемом контроллере.
            if (!method_exists($this->controller, $methodName))
            {
                $this->error = true;
            }
        }

        // если не найдено ни одного экшена - подставляем параметры error контроллера и 404 экшена.
        if ($this->error)
        {
            $methodName = str_replace("{action}", CONTROLLER_DEFAULT_ERROR_ACTION_404, $this::ACTION_PATTERN);
            $controllerName = str_replace("{controller}", CONTROLLER_DEFAULT_ERROR_CONTROLLER, $this::CONTROLLER_PATTERN);
            $this->controller = new $controllerName();
        }

        $this->action = $methodName;

        return $this;
    }

    private function _reflectionMethod()
    {
        $this->reflection = new \ReflectionMethod($this->controller, $this->action);

        foreach($this->reflection->getParameters() as $param)
        {
            if(isset($this->arguments[$param->getName()]))
            {
                $this->passion[] = $this->arguments[$param->getName()];
            }
            else
            {
                $this->passion[] = $param->getDefaultValue();
            }
        }

        return $this;
    }

    private function _httpmodule_OnStartExecute()
    {
        if (
            isset($this->site[$this::httpModule])
            &&
            is_array($this->site[$this::httpModule])
            &&
            !empty($this->site[$this::httpModule])
        )
        {
            $arr = $this->site[$this::httpModule];

            foreach($arr as $httpModule)
            {
                $httpModule = "\\HttpModules\\".$httpModule;
                $moduleObject = new $httpModule();

                if ($moduleObject instanceof \Kernel\IHttpModule)
                {
                    $moduleObject->OnStart();
                }
            }
        }
    }

    private function _setLocale()
    {
        if (isset($this->site["Locale"]))
        {
            $localeInt = -1;
            $localeArray = array();

            if (isset($this->site["Locale"][0]))
            {
                $localeInt = $this->site["Locale"][0];
            }

            if (isset($this->site["Locale"][1]) && is_array($this->site["Locale"][1]))
            {
                $localeArray = $this->site["Locale"][1];
            }

            if ($localeInt >= 0)
            {
                setlocale($localeInt, $localeArray);
            }
        }
    }

    public function Display()
    {
        $actionResult = $this->reflection->invokeArgs($this->controller, $this->passion);
        if ($actionResult instanceof \Kernel\IResponse)
        {
            print $actionResult->Render();
        }
        else
        {
            throw new \Exception("Action result does not instance of IResponse interface");
        }
    }

    /**
     * Get current site node
     * @return Site
     */
    static public function Start()
    {
        return new self();
    }
}