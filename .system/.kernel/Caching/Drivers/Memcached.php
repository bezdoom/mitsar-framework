<?php

namespace Kernel\Caching\Drivers;

class Memcached implements \Kernel\Caching\ICache
{
    private $memcache;
    private $timeLife;
    private $compress;

    public function currentCacheSystem()
    {
        return "Memcached";
    }

    /**
     *
     * @param string $host - хост сервера memcached
     * @param int $port - порт сервера memcached
     * @param int $compress - [0,1], сжимать или нет данные перед
     * помещением в память
     */
    public function __construct($host, $port = 11211, $compress = 0)
    {
        $this->memcache = memcache_connect($host, $port);
        $this->compress = ($compress) ? MEMCACHE_COMPRESSED : 0;
    }

    public function load($valueID, $timeLife)
    {
        $this->timeLife = $timeLife;
        return memcache_get($this->memcache, $valueID);
    }

    public function save($value, $valueID)
    {
        return memcache_set($this->memcache, $valueID, $value, $this->compress, $this->timeLife);
    }

    public function delete($valueID)
    {
        memcache_delete($this->memcache, $valueID);
    }

    public function __destruct()
    {
        memcache_close($this->memcache);
    }
}