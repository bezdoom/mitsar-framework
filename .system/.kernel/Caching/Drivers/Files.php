<?php

namespace Kernel\Caching\Drivers;

class Files implements \Kernel\Caching\ICache
{
    private $path;

    public function currentCacheSystem()
    {
        return "Files";
    }

    public function __construct($path = SYSTEM_FILES_CACHE_DIRECTORY)
    {
        $this->path = $path;
    }

    public function save($value, $valueID)
    {
        $str_val = serialize($value);
        $file_name = $this->pathCache($valueID) .
            $this->nameCache($valueID);
        $f = fopen($file_name, 'w+');
        if (flock($f, LOCK_EX)) {
            fwrite($f, $str_val);
            flock($f, LOCK_UN);
        }
        fclose($f);
        unset($str_val);
    }

    public function load($valueID, $time)
    {
        $file_name = $this->getPathCache($valueID) .
            $this->nameCache($valueID);
        if (!file_exists($file_name)) return false;
        if ((filemtime($file_name) + $time) < time()) {
            return false;
        }
        if (!$data = file($file_name))  return false;
        return unserialize(implode('', $data));
    }

    public function delete($valueID)
    {
        $file_name = $this->getPathCache($valueID) .
            $this->nameCache($valueID);
        unlink($file_name);
    }

    private function pathCache($valueID)
    {
        $md5 = $this->nameCache($valueID);
        $first_literal = array($md5{0}, $md5{1}, $md5{2}, $md5{3});
        $path = $this->path . DS;
        foreach ($first_literal as $dir) {
            $path .= $dir . DS;
            if (!file_exists(SYSTEM_ROOT_DIRECTORY . $path)) {
                if (!mkdir(SYSTEM_ROOT_DIRECTORY . $path, 0777, true)) return false;
            }
        }
        return SYSTEM_ROOT_DIRECTORY . $path;
    }

    private function getPathCache($valueID)
    {
        $md5 = $this->nameCache($valueID);
        $first_literal = array($md5{0}, $md5{1}, $md5{2}, $md5{3});
        return SYSTEM_ROOT_DIRECTORY . $this->path . DS .
            implode(DS, $first_literal) . DS;
    }

    private function nameCache($valueID)
    {
        return md5($valueID);
    }
}