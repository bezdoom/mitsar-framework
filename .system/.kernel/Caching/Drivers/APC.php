<?php

namespace Kernel\Caching\Drivers;

class APC implements \Kernel\Caching\ICache
{
    private $timeLife;

    public function currentCacheSystem()
    {
        return "APC";
    }

    public function load($valueID, $timeLife)
    {
        $this->timeLife = $timeLife;
        return apc_fetch($valueID);
    }

    public function save($value, $valueID)
    {
        return apc_store($valueID, $value, $this->timeLife);
    }

    public function delete($valueID)
    {
        return apc_delete($valueID);
    }
}