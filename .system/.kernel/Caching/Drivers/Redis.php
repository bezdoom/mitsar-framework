<?php

/*
 * TODO: Реализовать кеширование в Redis
 */

namespace Kernel\Caching\Drivers;

class Redis implements \Kernel\Caching\ICache
{
    public function currentCacheSystem()
    {
        return "Redis";
    }

    public function __construct()
    {
        // todo
    }

    public function load($valueID, $timeLife)
    {
        // todo
    }

    public function save($value, $valueID)
    {
        // todo
    }

    public function delete($valueID)
    {
        // todo
    }
}