<?php

namespace Kernel\Caching;

interface ICache
{
    public function currentCacheSystem();

    public function save($value, $valueID);
    public function load($valueID, $timeLife);
    public function delete($valueID);
}