<?php

namespace Kernel\Caching;

class Manager
{

    static protected $instance = null;

    static public function Node(\Kernel\Caching\ICache $cache)
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self($cache);
        }
        return self::$instance;
    }

    // === begin class ===

    private $_cache;

    public function __construct(\Kernel\Caching\ICache $cache)
    {
        $this->_cache = $cache;
    }

    public function load($valueID, $timeLife)
    {
        return $this->_cache->load($valueID, $timeLife);
    }

    public function save($value, $valueID)
    {
        $this->_cache->save($value, $valueID);
    }

    public function delete($valueID)
    {
        $this->_cache->delete($valueID);
    }

    public function currentCacheSystem()
    {
        return $this->_cache->currentCacheSystem();
    }
}