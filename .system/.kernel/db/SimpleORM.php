<?php

namespace Kernel\db;

use Kernel\DataAdapter;

class SimpleORM extends DataAdapter
{
    protected $orm_map = array();

    public function __get($name)
    {
        if (isset($this->orm_map[$name]))
        {
            return $this->orm_map[$name];
        }
        return null;
    }

    public function __set($name, $value)
    {
        $this->orm_map[$name] = $value;
    }
}