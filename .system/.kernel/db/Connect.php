<?php

namespace Kernel\db;

use \Kernel\db\PDO;

class Connect
{
    public $node = null;
    private $site_config = null;
    private $connection_config = null;

    static protected $instance = null;

    static public function Node()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // === begin class ===

    function __construct()
    {
        $this->Create();
    }

    public function Create()
    {
        if (is_null($this->connection_config))
        {
            $this->site_config = \Kernel\AppSettings::Node()->SiteArray();
            $this->connection_config = \Kernel\AppSettings::Node()->DBArray();
        }

        if (is_null($this->node))
        {
            try
            {
                $this->node = new PDO(
                    $this->connection_config["driver"].':host='.$this->connection_config["host"].';dbname='.$this->connection_config["dbname"],
                    $this->connection_config["dbuser"],
                    $this->connection_config["dbpasswd"]
                );

                if (isset($this->site_config["pdo_init"], $this->node) && is_array($this->site_config["pdo_init"]))
                {
                    foreach($this->site_config["pdo_init"] as $startup_init_query)
                    {
                        $this->node->exec($startup_init_query);
                    }
                }
            }
            catch (\Exception $e)
            {
                echo $e->getMessage();
            }
        }

        return $this;
    }
}