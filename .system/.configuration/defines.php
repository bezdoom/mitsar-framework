<?php

/*
 * System defines
 */
define ("PHP_VERSION_MIN", "5.3.0");
define ('DS', DIRECTORY_SEPARATOR);
define ("SYSTEM_CREATE_CACHE_FOLDER", true);
define ("SYSTEM_CACHE_DIRECTORY", ".cache");
define ("SYSTEM_FILES_CACHE_DIRECTORY", SYSTEM_CACHE_DIRECTORY . DS . ".stash");
define ("SYSTEM_DEFAULT_APPLICATION", "app");
define ("SYSTEM_DEFAULT_CHARSET", "utf8");
define ("SYSTEM_MBSTRING_DEFAULT_CHARSET", "UTF-8");
define ('SYSTEM_ROOT_DIRECTORY', dirname(dirname(dirname(__FILE__))) . DS);

/*
 * Twig defines
 */
define ("TWIG_CACHE_DIRECTORY", SYSTEM_CACHE_DIRECTORY . "/.twig/");
define ("TWIG_ENVIRONMENT_DIRECTORY", "{application}/View/");

/*
 * DateTime defines
 */
define ("DEFAULT_DATE_FORMAT", "Y-m-d");
define ("DEFAULT_TIME_FORMAT", "H:i:s");
define ("DEFAULT_DATETIME_FORMAT", "Y-m-d H:i");
define ("DEFAULT_TIMEZONE", "Europe/Moscow");

/*
 * Controller defines
 */
define ("CONTROLLER_DEFAULT_NAME", "Default");
define ("CONTROLLER_DEFAULT_ACTION", "Index");
define ("CONTROLLER_DEFAULT_ERROR_CONTROLLER", "Error");
define ("CONTROLLER_DEFAULT_ERROR_ACTION_404", "404");