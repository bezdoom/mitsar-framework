<?php

require_once(".autoload.php");
require_once(".ExternalLibraries/Twig/Autoloader.php");
require_once(".session_handler.php");

use Classes\Registry;

// Autoload Turn On
MitsarAutoLoader::Register(
    // TWIG autoload registry
    array("Twig_Autoloader", "autoload")
);

// Register AppSettings class
Registry::Set("SETTINGS", \Kernel\AppSettings::Node());

// Register Request
Registry::Set("URI", \Classes\Uri::Node());

// Register Cache
Registry::Set("CACHE",
    \Kernel\Caching\Manager::Node
    (
        // Здесь следует заменить кеширующий драйвер, если есть необходимость использовать APC, Memcached и тд
        new \Kernel\Caching\Drivers\Files()
    )
);

// Register PDO
Registry::Set('DB', \Kernel\db\Connect::Node()->node);

// HttpResponse Request
Registry::Set("RESPONSE", \Kernel\HttpResponse::Node());

// Register Request
Registry::Set("REQUEST", \Kernel\Request::Node());

// Register Twig Template data-model
Registry::Set("TWIG_MODEL", array());

// TWIG registry
// ===============
$arTwigSettings = array();
if (!defined('TWIG_DISABLE_CACHE')
    || constant('TWIG_DISABLE_CACHE') !== true)
{
    $arTwigSettings['cache'] = TWIG_CACHE_DIRECTORY;
}

// Twig Filesystem
Registry::Set("TWIG", new Twig_Environment(
    new Twig_Loader_Filesystem(
        str_replace("{application}", CURRENT_APPLICATION, TWIG_ENVIRONMENT_DIRECTORY)
    ),
    $arTwigSettings
));

// Twig String
Registry::Set('TWIG_STRING', new Twig_Environment(
    new Twig_Loader_String(),
    $arTwigSettings
));