<?php

require_once ".configuration/defines.php";
require_once ".kernel/.vanilla.php";
require_once ".bootstrap.php";

class MitsarAutoLoader
{
    const SYS_DIR = __DIR__;
    const PHP_EXTENSION = ".php";

    static function Load($class)
    {
        #region Patter prepare
        $search = array(
            "\\",
            "Kernel",
            "HttpModules"
        );
        $replace = array(
            DIRECTORY_SEPARATOR,
            ".kernel",
            "../" . CURRENT_APPLICATION . "/HttpModules"
        );

        $class = str_replace($search, $replace, $class);
        #endregion

        #region Controller prepare
        $class = preg_replace('#Controllers/(.*)Controller#i', "../" . CURRENT_APPLICATION . '/Controllers/$1', $class);
        #endregion

        $class_filename = self::SYS_DIR . DIRECTORY_SEPARATOR . $class . self::PHP_EXTENSION;
        if (file_exists($class_filename))
        {
            require_once ($class_filename);
        }
    }

    static public function Register()
    {
        spl_autoload_register(array("self", "Load"));

        $list = func_get_args();
        foreach($list as $registerString)
        {
            spl_autoload_register($registerString);
        }
    }
}