<?php

namespace HttpModules;

use Kernel\IHttpModule;
use Kernel\DataAdapter;

class BackslashRewrite extends DataAdapter implements IHttpModule
{
    /**
     * Выполняется при старте приложения
     */
    public function OnStart()
    {
        $strlen = mb_strlen($this->uri->full_path, SYSTEM_MBSTRING_DEFAULT_CHARSET) - 1;
        if(
            $this->uri->full_path !== "/" &&
            mb_substr($this->uri->full_path, $strlen, 1, SYSTEM_MBSTRING_DEFAULT_CHARSET) !== "/"
        )
        {
            $redirect_path = $this->uri->full_path . "/" . ($this->uri->query != false ? "?".$this->uri->query : "");
            $this->response->Redirect($redirect_path, 301);
        }
    }
}