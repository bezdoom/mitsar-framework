<?php

namespace Controllers;

use Kernel\Controller;
use Kernel\View;

class BlogsController extends Controller
{
    public function _actionIndex()
    {
        $this->SetVar("test", "Это тест");
        return new View('Blogs/index.twig');
    }
}