<?php

namespace Controllers;

use Kernel\Controller;
use Kernel\View;

class TestMethodController extends Controller
{
    public function __construct()
    {
        $this->SetVar("method", $_SERVER['REQUEST_METHOD']);
    }

    public function _actionIndex()
    {
        return new View('TestMethod/index.twig');
    }

    public function _actionUniversal()
    {
        return new View('TestMethod/universal.twig');
    }

    public function _get_actionGet()
    {
        return new View('TestMethod/get.twig');
    }

    public function _post_actionPost()
    {
        return new View('TestMethod/post.twig');
    }

    public function _put_actionPut()
    {
        return new View('TestMethod/put.twig');
    }

    public function _delete_actionDelete()
    {
        return new View('TestMethod/delete.twig');
    }

    public function _head_actionHead()
    {
        return new View('TestMethod/head.twig');
    }

    public function _options_actionOptions()
    {
        return new View('TestMethod/options.twig');
    }

    public function _patch_actionPatch()
    {
        return new View('TestMethod/patch.twig');
    }
}