<?php

namespace Controllers;

use Kernel\Controller;
use Kernel\View;

/**
 * Контроллер страниц ошибок
 */

class ErrorController extends Controller
{
    /**
     * Шаблонизация 403 ошибки
     * @return mixed
     */
    public function _action403()
    {
        $this->response->SetHeader(\Kernel\HttpResponse::HTTP_HEADER_403);
        return new View('_error/403.twig');
    }

    /**
     * Шаблонизация 404 ошибки
     * @return mixed
     */
    public function _action404()
    {
        $this->response->SetHeader(\Kernel\HttpResponse::HTTP_HEADER_404);
        return new View('_error/404.twig');
    }
}