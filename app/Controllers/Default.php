<?php

namespace Controllers;

use Kernel\Controller;
use Kernel\View;
use Kernel\Response;

class DefaultController extends Controller
{
    public function _actionIndex()
    {
        return new View('index.twig');
    }

    public function _actionResponse()
    {
        $this->SetVar("text", $this->request->get("name", null, "Чувак"));
        return new Response("Это просто репонс текст от твига, привет {{ text }}");
    }

    public function _actionDetail($NewsID)
    {
        pre($this->cache->currentCacheSystem());

        // пример использования кеша
        $cache_id = "mega" . $NewsID;

        $nocache = "";

        if (false === $var = $this->cache->load($cache_id, 10 * 6))
        {
            $nocache = "(NOT_CACHED) ";
            $var = new \Classes\DateTime();
            $var = $var->format("d M y h:i:s");
            $this->cache->save($var, $cache_id);
        }
        // конец примера

        $this->SetVar("nID", $NewsID . " | " . $nocache . $var);
        return new View('news.twig');
    }

    public function _actionPostForm()
    {
        return new View('form.twig');
    }

    public function _post_actionSubmit()
    {
        $this->SetVar("form", $_POST["form"]);
        return new View('form_result.twig');
    }
}