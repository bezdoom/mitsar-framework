<?php

/*
 * Routing MAP
 */

return array(

    "#^/news/([0-9]+)(/?)$#is"
    => array(
        "action" => "Detail",
        "controller" => "Default",
        "NewsID" => "$1"
    ),

    // TestMethod controller
    "#^/blogs(/?)$#is"
    => array(
        "controller" => "Blogs"
    ),

    // TestMethod controller
    "#^/test/method(/?)$#is"
    => array(
        "controller" => "TestMethod"
    ),
    "#^/test/method/([A-Za-z]+)(/?)$#is"
    => array(
        "action" => "$1",
        "controller" => "TestMethod"
    ),

    #region Default
    // Default route
    "#^/([A-Za-z0-9_]+)(/?)$#is"
    => array(
        "action" => "$1",
        "controller" => "Default"
    ),
    // Default route
    "#^/([A-Za-z0-9_]+)/([A-Za-z0-9_]+)(/?)$#is"
    => array(
        "action" => "$2",
        "controller" => "$1",
    )
    #endregion

);