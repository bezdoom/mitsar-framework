<?php

/**
 * Site global settings
 */

return array(
    "ID" => "app",
    "Title" => "Тестовый проект",
    "Language" => "ru",
    "DefaultCharset" => SYSTEM_DEFAULT_CHARSET,
    "Locale" => array(
        LC_ALL,
        array(
            'ru_RU.CP1251',
            'rus_RUS.CP1251',
            'Russian_Russia.1251',
            'russian'
        )
    ),

    // HttpModules
    "httpModules" => array(
        "BackslashRewrite"  // дописывает в конце ссылок закрывающий слеш (/) - выполняет SEO функции
    ),

    // PDO startUp array query-init
    "pdo_init" => array(
        "SET NAMES '".SYSTEM_DEFAULT_CHARSET."'",
        "SET CHARACTER SET '".SYSTEM_DEFAULT_CHARSET."'"
    )
);